no warnings;sub say(@) {my (undef, undef, $l) = caller; STDERR->say(join '', "|\n"x3, , qq(At line $l: ), @_)}
use Data::Dump qw(dump);
use Data::Edit::Xml;
=pod

A %hash in Perl looks up a value given a {key}.

Number It Ltd (stock symbol NIL) has kindly provided a look up table
to give the recommended Dita tag for each of their number tags:

=cut

my %ditaTags =                          # Declare the hash and load it
  qw(n1   concept   n4  p   CDATA CDATA
     n20  conbody   n80 ol
     n21  title     n81 li);            # Any even length list will do!

say q(%ditaTags == ), dump(\%ditaTags); # Print the loaded hash.

my $x = Data::Edit::Xml::new(<<END);    # NumberIT document to convert.
<n1>
  <n20>
  <n21>Latest disk drive</n21>
  <n4>Our new disk drive is better because it is:
    <n80>
    <n81>Faster</n81>  <n81>Cheaper</n81>  <n91>Smaller</n91>
    </n80>
  </n4>
  </n20>
</n1>
END

say 'NumberIT  == ', "\n", -p $x;      # Format and print the NumberIT document.

$x->by(sub                             # Look at each node in turn.
 {my ($o) = @_;                        # $o refers to the current node.,
  my $n = -t $o;                       # Get the current NumberIt tag from this node.

  if (my $d = $ditaTags{$n})           # Look up the value of the corresponding Dita tag
   {$o->change($d);                    # AND change the tag from NumberIt to Dita.
   }
  else                                 # No corresponding value found!
   {warn qq(No Dita tag for $n);       # Warn that the hash is incomplete!
   }
 });

say 'Dita  == ', "\n", -p $x;          # Format and print the Dita version of the document.

