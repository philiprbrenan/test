#Set up a GitLab Runner on AWS and direct work to it

##Create Runner
Follow instructions at: https://gitlab.com/philiprbrenan/test/settings/ci_cd to 
set up a shell runner and make sure that you tag the runner so that the work can 
be directed to the runner set up by using the tags: tag in .gitlab-ci.yml

##Adjust Runner Userid
On the computer running the gitlab-runner adjust the userid used by gitlib-runner
so that one can debug from the command line. Eg: on an AWS spot instance.

##Edit service definition file on runner noting user and working folder:
cat /etc/systemd/system/gitlab-runner.service
[Unit]
Description=GitLab Runner
After=syslog.target network.target
ConditionFileIsExecutable=/usr/local/bin/gitlab-runner

[Service]
StartLimitInterval=5
StartLimitBurst=10
ExecStart=/usr/local/bin/gitlab-runner "run" "--working-directory" "/home/phil/gitlab-runner" "--config" "/etc/gitlab-runner/config.toml" "--service" "gitlab-runner" "--syslog" "--user" "phil"





Restart=always
RestartSec=120

[Install]
WantedBy=multi-user.target

##Restart runner service

 sudo gitlab-runner restart & disown
 
##Verify runner,  also see: https://gitlab.com/philiprbrenan/test/settings/ci_cd  - expand runners   
  
  sudo gitlab-runner verify
  
##Set up the repository like this one.

##Publishes to the url below after about 4 minutes:

  https://philiprbrenan.gitlab.io/test/concept.html
